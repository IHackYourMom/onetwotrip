import React from 'react';
import { NavigatorIOS } from 'react-native';
import Login from './components/Login';

export default class App extends React.Component {
  constructor(props) {
    super(props);
    console.disableYellowBox = true;
  }
  render() {
    return (
      <NavigatorIOS
        initialRoute={{
          component: Login,
          title: 'Приоритеты'
        }}
        navigationBarHidden={true}
        style={{ flex: 1 }}
      />
    );
  }
} 