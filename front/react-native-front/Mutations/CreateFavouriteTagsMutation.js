import {
  commitMutation,
  graphql,
} from 'react-relay'
import environment from "../Environment"

const mutation = graphql`
  mutation CreateFavouriteTagsMutation($input: CreateFavouriteUserTagsInput! ){
    createFavouriteTags(input: $input) {
      favouriteTags {
        user {
          id
        }
      }
    }
  }
`

export default (tagClimate, tagKitchen, tagLandscape, callback) => {
  const variables = {
    input: {
      tagClimate,
      tagKitchen,
      tagLandscape,
    },
  }

  commitMutation(
    environment,
      {
        mutation,
        variables,
        onCompleted: () => {
          callback()
        },
        onError: err => console.error(err),
      }
  )
}
