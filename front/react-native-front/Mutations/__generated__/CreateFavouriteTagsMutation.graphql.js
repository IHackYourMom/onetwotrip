/**
 * @flow
 * @relayHash b10ba037e273f867fff5f1c1db6effef
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest } from 'relay-runtime';
export type CreateFavouriteUserTagsInput = {
  tagClimate?: ?string,
  tagKitchen?: ?string,
  tagLandscape?: ?string,
  clientMutationId?: ?string,
};
export type CreateFavouriteTagsMutationVariables = {|
  input: CreateFavouriteUserTagsInput
|};
export type CreateFavouriteTagsMutationResponse = {|
  +createFavouriteTags: ?{|
    +favouriteTags: ?{|
      +user: {|
        +id: string
      |}
    |}
  |}
|};
export type CreateFavouriteTagsMutation = {|
  variables: CreateFavouriteTagsMutationVariables,
  response: CreateFavouriteTagsMutationResponse,
|};
*/


/*
mutation CreateFavouriteTagsMutation(
  $input: CreateFavouriteUserTagsInput!
) {
  createFavouriteTags(input: $input) {
    favouriteTags {
      user {
        id
      }
      id
    }
  }
}
*/

const node/*: ConcreteRequest*/ = (function(){
var v0 = [
  {
    "kind": "LocalArgument",
    "name": "input",
    "type": "CreateFavouriteUserTagsInput!",
    "defaultValue": null
  }
],
v1 = [
  {
    "kind": "Variable",
    "name": "input",
    "variableName": "input",
    "type": "CreateFavouriteUserTagsInput!"
  }
],
v2 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "id",
  "args": null,
  "storageKey": null
},
v3 = {
  "kind": "LinkedField",
  "alias": null,
  "name": "user",
  "storageKey": null,
  "args": null,
  "concreteType": "UserType",
  "plural": false,
  "selections": [
    v2
  ]
};
return {
  "kind": "Request",
  "operationKind": "mutation",
  "name": "CreateFavouriteTagsMutation",
  "id": null,
  "text": "mutation CreateFavouriteTagsMutation(\n  $input: CreateFavouriteUserTagsInput!\n) {\n  createFavouriteTags(input: $input) {\n    favouriteTags {\n      user {\n        id\n      }\n      id\n    }\n  }\n}\n",
  "metadata": {},
  "fragment": {
    "kind": "Fragment",
    "name": "CreateFavouriteTagsMutation",
    "type": "Mutation",
    "metadata": null,
    "argumentDefinitions": v0,
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "createFavouriteTags",
        "storageKey": null,
        "args": v1,
        "concreteType": "CreateFavouriteUserTagsPayload",
        "plural": false,
        "selections": [
          {
            "kind": "LinkedField",
            "alias": null,
            "name": "favouriteTags",
            "storageKey": null,
            "args": null,
            "concreteType": "FavouriteTagsNode",
            "plural": false,
            "selections": [
              v3
            ]
          }
        ]
      }
    ]
  },
  "operation": {
    "kind": "Operation",
    "name": "CreateFavouriteTagsMutation",
    "argumentDefinitions": v0,
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "createFavouriteTags",
        "storageKey": null,
        "args": v1,
        "concreteType": "CreateFavouriteUserTagsPayload",
        "plural": false,
        "selections": [
          {
            "kind": "LinkedField",
            "alias": null,
            "name": "favouriteTags",
            "storageKey": null,
            "args": null,
            "concreteType": "FavouriteTagsNode",
            "plural": false,
            "selections": [
              v3,
              v2
            ]
          }
        ]
      }
    ]
  }
};
})();
// prettier-ignore
(node/*: any*/).hash = 'd257cc9707a46ef2516401ff21af0a66';
module.exports = node;
