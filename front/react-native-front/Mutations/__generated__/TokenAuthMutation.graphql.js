/**
 * @flow
 * @relayHash 9c166dff2f7fd44a6886d355199d411d
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest } from 'relay-runtime';
export type ObtainJSONWebTokenInput = {
  username: string,
  password: string,
  clientMutationId?: ?string,
};
export type TokenAuthMutationVariables = {|
  input: ObtainJSONWebTokenInput
|};
export type TokenAuthMutationResponse = {|
  +tokenAuth: ?{|
    +token: ?string
  |}
|};
export type TokenAuthMutation = {|
  variables: TokenAuthMutationVariables,
  response: TokenAuthMutationResponse,
|};
*/


/*
mutation TokenAuthMutation(
  $input: ObtainJSONWebTokenInput!
) {
  tokenAuth(input: $input) {
    token
  }
}
*/

const node/*: ConcreteRequest*/ = (function(){
var v0 = [
  {
    "kind": "LocalArgument",
    "name": "input",
    "type": "ObtainJSONWebTokenInput!",
    "defaultValue": null
  }
],
v1 = [
  {
    "kind": "LinkedField",
    "alias": null,
    "name": "tokenAuth",
    "storageKey": null,
    "args": [
      {
        "kind": "Variable",
        "name": "input",
        "variableName": "input",
        "type": "ObtainJSONWebTokenInput!"
      }
    ],
    "concreteType": "ObtainJSONWebTokenPayload",
    "plural": false,
    "selections": [
      {
        "kind": "ScalarField",
        "alias": null,
        "name": "token",
        "args": null,
        "storageKey": null
      }
    ]
  }
];
return {
  "kind": "Request",
  "operationKind": "mutation",
  "name": "TokenAuthMutation",
  "id": null,
  "text": "mutation TokenAuthMutation(\n  $input: ObtainJSONWebTokenInput!\n) {\n  tokenAuth(input: $input) {\n    token\n  }\n}\n",
  "metadata": {},
  "fragment": {
    "kind": "Fragment",
    "name": "TokenAuthMutation",
    "type": "Mutation",
    "metadata": null,
    "argumentDefinitions": v0,
    "selections": v1
  },
  "operation": {
    "kind": "Operation",
    "name": "TokenAuthMutation",
    "argumentDefinitions": v0,
    "selections": v1
  }
};
})();
// prettier-ignore
(node/*: any*/).hash = '84afff6196322f5a3dd6ee434b08dde7';
module.exports = node;
