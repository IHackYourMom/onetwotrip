type AirportNode implements Node {
  # The ID of the object.
  id: ID!
  name: String!
  country: CountryNode!
  city: CityNode!
  ticketsFromAirport(before: String, after: String, first: Int, last: Int): TicketNodeConnection
  ticketsToAirport(before: String, after: String, first: Int, last: Int): TicketNodeConnection
}

type AirportNodeConnection {
  pageInfo: PageInfo!
  edges: [AirportNodeEdge]!
}

type AirportNodeEdge {
  # The item at the end of the edge
  node: AirportNode

  # A cursor for use in pagination
  cursor: String!
}

type CityNode implements Node {
  # The ID of the object.
  id: ID!
  name: String!
  country: CountryNode!
  airportsCity(before: String, after: String, first: Int, last: Int): AirportNodeConnection
}

type CityNodeConnection {
  pageInfo: PageInfo!
  edges: [CityNodeEdge]!
}

type CityNodeEdge {
  # The item at the end of the edge
  node: CityNode

  # A cursor for use in pagination
  cursor: String!
}

type CountryNode implements Node {
  # The ID of the object.
  id: ID!
  name: String!
  tagClimate: String!
  tagKitchen: String!
  tagLandscape: String!
  cities(before: String, after: String, first: Int, last: Int): CityNodeConnection
  airportsContry(before: String, after: String, first: Int, last: Int): AirportNodeConnection
}

type CountryNodeConnection {
  pageInfo: PageInfo!
  edges: [CountryNodeEdge]!
}

type CountryNodeEdge {
  # The item at the end of the edge
  node: CountryNode

  # A cursor for use in pagination
  cursor: String!
}

input CreateDislikeInput {
  ticketId: ID
  clientMutationId: String
}

type CreateDislikePayload {
  dislike: DislikeNode
  clientMutationId: String
}

input CreateFavouriteUserTagsInput {
  tagClimate: String
  tagKitchen: String
  tagLandscape: String
  clientMutationId: String
}

type CreateFavouriteUserTagsPayload {
  favouriteTags: FavouriteTagsNode
  clientMutationId: String
}

input CreateLikeInput {
  ticketId: ID
  clientMutationId: String
}

type CreateLikePayload {
  like: LikeNode
  clientMutationId: String
}

input CreateUserInput {
  username: String!
  password: String!
  email: String!
  clientMutationId: String
}

type CreateUserPayload {
  user: UserNode
  clientMutationId: String
}

# The `DateTime` scalar type represents a DateTime
# value as specified by
# [iso8601](https://en.wikipedia.org/wiki/ISO_8601).
scalar DateTime

type DislikeNode implements Node {
  # The ID of the object.
  id: ID!
  user: UserType!
  ticket: TicketNode!
}

type DislikeNodeConnection {
  pageInfo: PageInfo!
  edges: [DislikeNodeEdge]!
}

type DislikeNodeEdge {
  # The item at the end of the edge
  node: DislikeNode

  # A cursor for use in pagination
  cursor: String!
}

type FavouriteTagsNode implements Node {
  # The ID of the object.
  id: ID!
  user: UserType!
  tagClimate: String!
  tagKitchen: String!
  tagLandscape: String!
  tags(before: String, after: String, first: Int, last: Int): UserDataSetConnection
}

type FavouriteTagsNodeConnection {
  pageInfo: PageInfo!
  edges: [FavouriteTagsNodeEdge]!
}

type FavouriteTagsNodeEdge {
  # The item at the end of the edge
  node: FavouriteTagsNode

  # A cursor for use in pagination
  cursor: String!
}

# The `GenericScalar` scalar type represents a generic
# GraphQL scalar value that could be:
# String, Boolean, Int, Float, List or Object.
scalar GenericScalar

type LikeNode implements Node {
  # The ID of the object.
  id: ID!
  user: UserType!
  ticket: TicketNode!
}

type LikeNodeConnection {
  pageInfo: PageInfo!
  edges: [LikeNodeEdge]!
}

type LikeNodeEdge {
  # The item at the end of the edge
  node: LikeNode

  # A cursor for use in pagination
  cursor: String!
}

type Mutation {
  createLike(input: CreateLikeInput!): CreateLikePayload
  createDislike(input: CreateDislikeInput!): CreateDislikePayload
  createFavouriteTags(input: CreateFavouriteUserTagsInput!): CreateFavouriteUserTagsPayload
  createUser(input: CreateUserInput!): CreateUserPayload
  tokenAuth(input: ObtainJSONWebTokenInput!): ObtainJSONWebTokenPayload
  verifyToken(input: VerifyInput!): VerifyPayload
  refreshToken(input: RefreshInput!): RefreshPayload
}

# An object with an ID
interface Node {
  # The ID of the object.
  id: ID!
}

input ObtainJSONWebTokenInput {
  username: String!
  password: String!
  clientMutationId: String
}

# Obtain JSON Web Token mutation
type ObtainJSONWebTokenPayload {
  token: String
  clientMutationId: String
}

type PageInfo {
  # When paginating forwards, are there more items?
  hasNextPage: Boolean!

  # When paginating backwards, are there more items?
  hasPreviousPage: Boolean!

  # When paginating backwards, the cursor to continue.
  startCursor: String

  # When paginating forwards, the cursor to continue.
  endCursor: String
}

type Query {
  # The ID of the object
  country(id: ID!): CountryNode
  countries(before: String, after: String, first: Int, last: Int, name: String, tagClimate: String, tagKitchen: String, tagLandscape: String): CountryNodeConnection

  # The ID of the object
  city(id: ID!): CityNode
  cities(before: String, after: String, first: Int, last: Int, name: String, country: ID): CityNodeConnection

  # The ID of the object
  airport(id: ID!): AirportNode
  airports(before: String, after: String, first: Int, last: Int, name: String, city: ID): AirportNodeConnection

  # The ID of the object
  ticket(id: ID!): TicketNode
  tickets(before: String, after: String, first: Int, last: Int, minPrize: Float, fromAirport: ID, toAirport: ID): TicketNodeConnection

  # The ID of the object
  like(id: ID!): LikeNode
  likes(before: String, after: String, first: Int, last: Int, ticket: ID, user: ID): LikeNodeConnection

  # The ID of the object
  dislike(id: ID!): DislikeNode
  dislikes(before: String, after: String, first: Int, last: Int, ticket: ID, user: ID): DislikeNodeConnection
  favouriteTags(before: String, after: String, first: Int, last: Int, tagClimate: String, tagKitchen: String, tagLandscape: String): FavouriteTagsNodeConnection

  # The ID of the object
  user(id: ID!): UserNode
  users(before: String, after: String, first: Int, last: Int, username: String, email: String): UserNodeConnection
}

input RefreshInput {
  token: String
  clientMutationId: String
}

type RefreshPayload {
  token: String
  payload: GenericScalar
  clientMutationId: String
}

type TicketNode implements Node {
  # The ID of the object.
  id: ID!
  minPrize: Int!
  fromAirport: AirportNode!
  toAirport: AirportNode!
  likes(before: String, after: String, first: Int, last: Int): LikeNodeConnection
  dislikes(before: String, after: String, first: Int, last: Int): DislikeNodeConnection
}

type TicketNodeConnection {
  pageInfo: PageInfo!
  edges: [TicketNodeEdge]!
}

type TicketNodeEdge {
  # The item at the end of the edge
  node: TicketNode

  # A cursor for use in pagination
  cursor: String!
}

type UserDataSet implements Node {
  # The ID of the object.
  id: ID!
  user: UserType!
  favouriteTags: FavouriteTagsNode!
  userJsonPath: String!
}

type UserDataSetConnection {
  pageInfo: PageInfo!
  edges: [UserDataSetEdge]!
}

type UserDataSetEdge {
  # The item at the end of the edge
  node: UserDataSet

  # A cursor for use in pagination
  cursor: String!
}

type UserNode implements Node {
  # The ID of the object.
  id: ID!
  password: String!
  lastLogin: DateTime

  # Designates that this user has all permissions without explicitly assigning them.
  isSuperuser: Boolean!

  # Required. 150 characters or fewer. Letters, digits and @/./+/-/_ only.
  username: String!
  firstName: String!
  lastName: String!
  email: String!

  # Designates whether the user can log into this admin site.
  isStaff: Boolean!

  # Designates whether this user should be treated as active. Unselect this instead of deleting accounts.
  isActive: Boolean!
  dateJoined: DateTime!
  favouritetagsSet(before: String, after: String, first: Int, last: Int): FavouriteTagsNodeConnection
  userlikeSet(before: String, after: String, first: Int, last: Int): LikeNodeConnection
  userdislikeSet(before: String, after: String, first: Int, last: Int): DislikeNodeConnection
  userdatasetSet(before: String, after: String, first: Int, last: Int): UserDataSetConnection
}

type UserNodeConnection {
  pageInfo: PageInfo!
  edges: [UserNodeEdge]!
}

type UserNodeEdge {
  # The item at the end of the edge
  node: UserNode

  # A cursor for use in pagination
  cursor: String!
}

type UserType {
  id: ID!
  password: String!
  lastLogin: DateTime

  # Designates that this user has all permissions without explicitly assigning them.
  isSuperuser: Boolean!

  # Required. 150 characters or fewer. Letters, digits and @/./+/-/_ only.
  username: String!
  firstName: String!
  lastName: String!
  email: String!

  # Designates whether the user can log into this admin site.
  isStaff: Boolean!

  # Designates whether this user should be treated as active. Unselect this instead of deleting accounts.
  isActive: Boolean!
  dateJoined: DateTime!
  favouritetagsSet(before: String, after: String, first: Int, last: Int): FavouriteTagsNodeConnection
  userlikeSet(before: String, after: String, first: Int, last: Int): LikeNodeConnection
  userdislikeSet(before: String, after: String, first: Int, last: Int): DislikeNodeConnection
  userdatasetSet(before: String, after: String, first: Int, last: Int): UserDataSetConnection
}

input VerifyInput {
  token: String
  clientMutationId: String
}

type VerifyPayload {
  payload: GenericScalar
  clientMutationId: String
}

