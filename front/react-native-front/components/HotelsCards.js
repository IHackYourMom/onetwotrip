import React from 'react';
import { View, Button, Text, StyleSheet } from 'react-native';
import Swiper from 'react-native-deck-swiper';
import HotelCard from './HotelCard';
import FavoriteHotels from './FavoriteHotels';

class HotelsCards extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isEndReached: false,
      favorites: [],
      unwanted: []
    };
    this._onSwipedLeft = this._onSwipedLeft.bind(this);
    this._onSwipedRight = this._onSwipedRight.bind(this);
    this._onSwipedAll = this._onSwipedAll.bind(this);
    this._onGotoFavoritesPress = this._onGotoFavoritesPress.bind(this);
  }

  _onSwipedLeft(cardIndex) {
    console.log('Swiped left ' + cardIndex);
    this.setState({ unwanted: [...this.state.unwanted, hotels[cardIndex]] });
  }

  _onSwipedRight(cardIndex) {
    console.log('Swiped right ' + cardIndex);
    this.setState({ favorites: [...this.state.favorites, hotels[cardIndex]] });
  }

  _onSwipedAll() {
    this.setState({ isEndReached: true });
  }

  _onGotoFavoritesPress() {
    const favoriteHotelsScreen = {
      component: FavoriteHotels,
      title: 'Избранные отели',
      navigationBarHidden: false,
      passProps: { favorites: this.state.favorites }
    };
    this.props.navigator.push(favoriteHotelsScreen);
  }

  render() {
    if (this.state.isEndReached) return (
      <View style={styles.container}>
        <Text style={styles.noMore}>Больше отелей нет</Text>
        <Button title="Посмотреть избранные отели"
          onPress={() => this._onGotoFavoritesPress()}
          color="#fcc62e" />
      </View>
    );
    return (
      <View style={styles.container}>
        <Swiper cards={hotels}
          renderCard={card => <HotelCard card={card} />}
          onSwipedLeft={cardIndex => this._onSwipedLeft(cardIndex)}
          onSwipedRight={cardIndex => this._onSwipedRight(cardIndex)}
          onSwipedAll={() => this._onSwipedAll()}
          cardIndex={0}
          backgroundColor="#222222"
          stackSize={3}
          verticalSwipe={false} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 30,
    backgroundColor: '#222222'
  },
  noMore: {
    color: '#ffffff',
    fontSize: 30,
    textAlign: 'center'
  }
});

const hotels = [
  {
    imageUrl: 'https://foto.hrsstatic.com/fotos/0/3/256/256/80/FFFFFF/http%3A%2F%2Ffoto-origin.hrsstatic.com%2Ffoto%2F0%2F8%2F4%2F0%2F%2Fteaser_084098.jpg/0/512%2C298/6/Sheraton_Amman_Al_Nabil_Hotel-Amman-Exterior_view-9-84098.jpg',
    name: 'Sheraton Amman Hotel',
    rating: '9.4',
    price: '7.500 ₽'
  },
  {
    imageUrl: 'http://www.airehotelalmeria.com/wp-content/uploads/2017/03/AIRE_Almeria_Hotel_148-1-256x256.jpg',
    name: 'AIRE Hotel',
    rating: '9.6',
    price: '4.200 ₽'
  },
  {
    imageUrl: 'http://www.airehotelalmeria.com/wp-content/uploads/2017/03/AIRE_Almeria_Hotel_722-1-256x256.jpg',
    name: 'Haddars SPA',
    rating: '9.9',
    price: '6.700 ₽'
  },
  {
    imageUrl: 'https://foto.hrsstatic.com/fotos/0/3/256/256/80/FFFFFF/http%3A%2F%2Ffoto-origin.hrsstatic.com%2Ffoto%2F1%2F5%2F3%2F0%2F153049%2F153049_a_1577585.jpg/fkilWyy5WZRkxgyQl2yGLA%3D%3D/128%2C127/6/Monte_Carlo_Hotel-Alanya-Exterior_view-1-153049.jpg',
    name: 'Ultra Super Mega Hotel',
    rating: '5.2',
    price: '10.660 ₽'
  },
  {
    imageUrl: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQCtbVgAezMULq2OvEvtaX4PBW09RNsd1xijkvIGi4MndsdcW9J',
    name: 'Hotel Cantemeria',
    rating: '8.4',
    price: '12.540 ₽'
  },
  {
    imageUrl: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQu5l19n7lYFMy_fGWbELdkYtk3d3ZNWgRusplzScTGbTc3IDe_WA',
    name: 'Vidhya AH',
    rating: '7.8',
    price: '11.300 ₽'
  }
];

export default HotelsCards;