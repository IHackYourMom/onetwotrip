import React from 'react';
import { View, Text, ImageBackground, StyleSheet } from 'react-native';

class HotelCard extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { imageUrl, name, rating, price } = this.props.card;
    return (
      <ImageBackground style={styles.card}
        source={{ uri: imageUrl }}>
        <View style={styles.cardTextContainer}>
          <Text style={styles.additional}>Рейтинг: {rating}</Text>
          <Text style={styles.city}>{name}</Text>
        </View>
        <Text style={styles.price}>{price}</Text>
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  card: {
    flex: 1,
    borderRadius: 4,
    borderWidth: 2,
    borderColor: 'grey',
    justifyContent: 'center',
    backgroundColor: 'white'
  },
  cardTextContainer: {
    padding: 10
  },
  additional: {
    color: '#ffffff',
    fontSize: 15
  },
  city: {
    color: '#ffffff',
    fontSize: 40,
    fontWeight: '500'
  },
  price: {
    alignContent: 'center',
    color: '#ffffff',
    fontSize: 40,
    padding: 10,
    fontWeight: '500'
  }
});

export default HotelCard;