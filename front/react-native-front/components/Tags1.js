import React from 'react';
import { View, Text, Button, StyleSheet } from 'react-native';
import { CheckBox } from 'react-native-elements';
import Cards from './Cards';

class Tags extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      sea: false,
      sands: false,
      mountains: false,
      forest: false,
      meat: false,
      vegan: false,
      hot: false,
      standart: false,
      cold: false
    };
  }

  render() {
    return (
      <View style={styles.view}>
        <Text style={styles.viewtitle}>
          Выберите приоритеты для путешествия:
        </Text>
        <Text style={styles.paramtitle}>
          Ландшафт
        </Text>
        <View style={{ flexDirection: 'row', justifyContent: 'space-around' }}>
          <CheckBox title="Море"
            checked={this.state.sea}
            checkedColor="#fcc62e"
            uncheckedColor="#fcc62e"
            onPress={() => this.setState({ sea: !this.state.sea })}
            containerStyle={styles.checkbox}
            textStyle={{ color: '#fcc62e' }} />
          <CheckBox title="Пески"
            checked={this.state.sands}
            checkedColor="#fcc62e"
            uncheckedColor="#fcc62e"
            onPress={() => this.setState({ sands: !this.state.sands })}
            containerStyle={styles.checkbox}
            textStyle={{ color: '#fcc62e' }} />
          <CheckBox title="Горы"
            checked={this.state.mountains}
            checkedColor="#fcc62e"
            uncheckedColor="#fcc62e"
            onPress={() => this.setState({ mountains: !this.state.mountains })}
            containerStyle={styles.checkbox}
            textStyle={{ color: '#fcc62e' }} />
          <CheckBox title="Лес"
            checked={this.state.forest}
            checkedColor="#fcc62e"
            uncheckedColor="#fcc62e"
            onPress={() => this.setState({ forest: !this.state.forest })}
            containerStyle={styles.checkbox}
            textStyle={{ color: '#fcc62e' }} />
        </View>
        <Text style={styles.paramtitle}>
          Кухня
        </Text>
        <View style={{ flexDirection: 'row', justifyContent: 'space-around' }}>
          <CheckBox title="Мясо"
            checked={this.state.meat}
            checkedColor="#fcc62e"
            uncheckedColor="#fcc62e"
            onPress={() => this.setState({ meat: !this.state.meat })}
            containerStyle={styles.checkbox}
            textStyle={{ color: '#fcc62e' }} />
          <CheckBox title="Веганская"
            checked={this.state.vegan}
            checkedColor="#fcc62e"
            uncheckedColor="#fcc62e"
            onPress={() => this.setState({ vegan: !this.state.vegan })}
            containerStyle={styles.checkbox}
            textStyle={{ color: '#fcc62e' }} />
        </View>
        <Text style={styles.paramtitle}>
          Климат
        </Text>
        <View style={{ flexDirection: 'row', justifyContent: 'space-around' }}>
          <CheckBox title="Жарко"
            checked={this.state.hot}
            checkedColor="#fcc62e"
            uncheckedColor="#fcc62e"
            onPress={() => this.setState({ hot: !this.state.hot })}
            containerStyle={styles.checkbox}
            textStyle={{ color: '#fcc62e' }} />
          <CheckBox title="Нормально"
            checked={this.state.standart}
            checkedColor="#fcc62e"
            uncheckedColor="#fcc62e"
            onPress={() => this.setState({ standart: !this.state.standart })}
            containerStyle={styles.checkbox}
            textStyle={{ color: '#fcc62e' }} />
          <CheckBox title="Холодно"
            checked={this.state.cold}
            checkedColor="#fcc62e"
            uncheckedColor="#fcc62e"
            onPress={() => this.setState({ cold: !this.state.cold })}
            containerStyle={styles.checkbox}
            textStyle={{ color: '#fcc62e' }} />
        </View>
        <Button onPress={() => this.props.navigator.push(cardsScreen)}
          title="Показать направления"
          color="#fcc62e" />
      </View>
    );
  }
}

const cardsScreen = {
  component: Cards,
  title: 'Направления',
  navigationBarHidden: false,
  passProps: {
    cards: [
      {
        imageUrl: 'http://image1.thematicnews.com/uploads/topics/preview/00/10/37/12/8e7672590e_256crop.jpg',
        country: 'Германия',
        city: 'Мюнхен',
        dates: '10-17 ноя',
        price: '5.500 ₽'
      },
      {
        imageUrl: 'https://media.ticmate.com/resources/ticmate_live/upload/London_Billetter_Tower_of_London_Main.jpg',
        country: 'Англия',
        city: 'Лондон',
        dates: '1-9 дек',
        price: '5.350 ₽'
      },
      {
        imageUrl: 'https://foto.hrsstatic.com/fotos/0/3/256/256/80/FFFFFF/http%3A%2F%2Ffoto-origin.hrsstatic.com%2Ffoto%2F4%2F0%2F3%2F0%2F403043%2F403043_a_490787.jpg/MDw7bM%2FNOEMbXp04c3lzBw%3D%3D/0%2C0/6/Sarotti_Hoefe-Berlin-Exterior_view-3-403043.jpg',
        country: 'Германия',
        city: 'Берлин',
        dates: '4-11 дек',
        price: '3.200 ₽'
      },
      {
        imageUrl: 'https://static-s.aa-cdn.net/img/gp/20600004652650/Dr60urxAGWz6QBx9FCLfpFprx5tW_DU_-b53H2ZZrZrjAyvG7Tfnp37ggU2yRHJ-Rfnc=w300?v=1',
        country: 'Нидерланды',
        city: 'Амстердам',
        dates: '10-22 дек',
        price: '7.200 ₽'
      },
      {
        imageUrl: 'https://c.tcdn.co/852/9de/8529dee2-38d8-11e6-bbbf-040157cdaf01/channel256.png',
        country: 'Италия',
        city: 'Рим',
        dates: '1-7 дек',
        price: '6.540 ₽'
      }
    ]
  }
};

const styles = StyleSheet.create({
  view: {
    flex: 1,
    marginTop: 20,
    paddingTop: 20,
    paddingLeft: 10,
    paddingRight: 10,
    backgroundColor: '#222222'
  },
  viewtitle: {
    color: '#fcc62e',
    fontWeight: 'bold',
    fontSize: 25,
    marginBottom: 20
  },
  paramtitle: {
    color: '#fcc62e',
    fontSize: 20,
    marginBottom: 10,
    alignSelf: 'center'
  },
  checkbox: {
    backgroundColor: '#222222',
    borderWidth: 0
  }
});

export default Tags;