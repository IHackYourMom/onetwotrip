import React from 'react';
import { ActivityIndicator, Alert, View, Picker, Image, Text, TextInput, Button, StyleSheet } from 'react-native';
import logo from '../assets/onetwotrip-logo.png';
import Tags from './Tags';
import Register from './Register';
import TokenAuthMutation from '../Mutations/TokenAuthMutation';

class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      isLoggedIn: false,
      username: '',
      password: ''
    };
    this._onLoginButtonClick = this._onLoginButtonClick.bind(this);
    this._onRegisterButtonClick = this._onRegisterButtonClick.bind(this);
  }

  _onLoginButtonClick() {
    this.setState({ isLoading: true }, () => {
      const { username, password } = this.state;
      TokenAuthMutation(username, password, (token) => {
        console.log(token);
        this.setState({ isLoading: false });
        this.setState({
          isLoggedIn: true,
          username: '',
          password: ''
        });
      });
    });
  }

  _onRegisterButtonClick() {
    const registerScreen = {
      component: Register,
      title: 'Регистрация',
      navigationBarHidden: false
    };
    this.props.navigator.push(registerScreen);
  }

  render() {
    if (this.state.isLoggedIn) return <Tags navigator={this.props.navigator} />;
    else return (
      <View style={styles.loginform}>
        <Image style={styles.logo}
          source={logo} />
        <TextInput style={styles.textbox}
          placeholder="Логин"
          placeholderTextColor="grey"
          onChangeText={text => this.setState({ username: text })}
          value={this.state.username}
          autoCapitalize="none"
          returnKeyType="next"
          onSubmitEditing={() => this.passwordTextInput.focus()}
          blurOnSubmit={false} />
        <TextInput style={styles.textbox}
          placeholder="Пароль"
          placeholderTextColor="grey"
          onChangeText={text => this.setState({ password: text })}
          value={this.state.password}
          secureTextEntry={true}
          returnKeyType="done"
          onSubmitEditing={() => this._onLoginButtonClick()}
          ref={input => this.passwordTextInput = input} />
        <View style={{ flexDirection: 'row', justifyContent: 'space-evenly' }}>
          <Button onPress={() => this._onLoginButtonClick()}
            title="Войти"
            color="#fcc62e" />
          <Button onPress={() => this._onRegisterButtonClick()}
            title="Регистрация"
            color="#fcc62e" />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  loginform: {
    flex: 1,
    marginTop: 20,
    paddingTop: 20,
    backgroundColor: '#222222'
  },
  logo: {
    width: 150,
    height: 150,
    alignSelf: 'center'
  },
  textbox: {
    height: 30,
    borderColor: 'grey',
    borderWidth: 1,
    fontSize: 20,
    marginHorizontal: 15,
    marginBottom: 10,
    color: '#ffffff'
  }
});

export default Login;