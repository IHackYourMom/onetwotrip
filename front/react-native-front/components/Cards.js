import React from 'react';
import { View, Text, Button, StyleSheet } from 'react-native';
import Swiper from 'react-native-deck-swiper';
import Card from './Card';
import Favorites from './Favorites';

class Cards extends React.Component {
  constructor(props) {
    super(props);
    const isEndReached = !(this.props.cards.length > 0);
    this.state = {
      isEndReached,
      favorites: [],
      unwanted: []
    };
    this._onSwipedLeft = this._onSwipedLeft.bind(this);
    this._onSwipedRight = this._onSwipedRight.bind(this);
    this._onSwipedAll = this._onSwipedAll.bind(this);
    this._onGotoFavoritesPress = this._onGotoFavoritesPress.bind(this);
  }

  _onSwipedLeft(cardIndex) {
    console.log('Swiped left ' + cardIndex);
    this.setState({ unwanted: [...this.state.unwanted, this.props.cards[cardIndex]] });
  }

  _onSwipedRight(cardIndex) {
    console.log('Swiped right ' + cardIndex);
    this.setState({ favorites: [...this.state.favorites, this.props.cards[cardIndex]] });
  }

  _onSwipedAll() {
    this.setState({ isEndReached: true });
  }

  _onGotoFavoritesPress() {
    const favoritesScreen = {
      component: Favorites,
      title: 'Избранные направления',
      navigationBarHidden: false,
      passProps: { favorites: this.state.favorites }
    };
    this.props.navigator.push(favoritesScreen);
  }

  render() {
    if (this.state.isEndReached) return (
      <View style={styles.container}>
        <Text style={styles.noMore}>Больше направлений нет</Text>
        <Button title="Посмотреть избранные направления"
          onPress={() => this._onGotoFavoritesPress()}
          color="#fcc62e" />
      </View>
    );
    return (
      <View style={styles.container}>
        <Swiper cards={this.props.cards}
          renderCard={card => <Card card={card} />}
          onSwipedLeft={cardIndex => this._onSwipedLeft(cardIndex)}
          onSwipedRight={cardIndex => this._onSwipedRight(cardIndex)}
          onSwipedAll={() => this._onSwipedAll()}
          cardIndex={0}
          backgroundColor="#222222"
          stackSize={3}
          verticalSwipe={false} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 30,
    backgroundColor: '#222222'
  },
  noMore: {
    color: '#ffffff',
    fontSize: 30,
    textAlign: 'center'
  }
});

export default Cards;