import React from 'react';
import { ActivityIndicator, View, TextInput, Button, StyleSheet } from 'react-native';
import CreateUserMutation from '../Mutations/CreateUserMutation';

class Register extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      username: '',
      email: '',
      password: ''
    };
    this._onRegisterButtonClick = this._onRegisterButtonClick.bind(this);
  }

  _onRegisterButtonClick() {
    this.setState({ isLoading: true }, () => {
      const { username, email, password } = this.state;
      CreateUserMutation(username, email, password, () => {
        this.props.navigator.pop();
        this.setState({ isLoading: false });
      });
    })
  }

  render() {
    return (
      <View style={styles.loginform}>
        <ActivityIndicator animating={this.state.isLoading} />
        <TextInput style={styles.textbox}
          placeholder="Логин"
          placeholderTextColor="grey"
          onChangeText={text => this.setState({ username: text })}
          value={this.state.username}
          autoCapitalize="none"
          returnKeyType="next"
          onSubmitEditing={() => this.emailTextInput.focus()}
          blurOnSubmit={false} />
        <TextInput style={styles.textbox}
          placeholder="Почта"
          placeholderTextColor="grey"
          onChangeText={text => this.setState({ email: text })}
          value={this.state.email}
          autoCapitalize="none"
          returnKeyType="next"
          keyboardType="email-address"
          onSubmitEditing={() => this.passwordTextInput.focus()}
          blurOnSubmit={false}
          ref={input => this.emailTextInput = input} />
        <TextInput style={styles.textbox}
          placeholder="Пароль"
          placeholderTextColor="grey"
          onChangeText={text => this.setState({ password: text })}
          value={this.state.password}
          secureTextEntry={true}
          returnKeyType="done"
          onSubmitEditing={() => this._onRegisterButtonClick()}
          ref={input => this.passwordTextInput = input} />
        <Button onPress={() => this._onRegisterButtonClick()}
          title="Зарегистрироваться"
          color="#fcc62e" />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  loginform: {
    flex: 1,
    marginTop: 60,
    paddingTop: 20,
    backgroundColor: '#222222'
  },
  textbox: {
    height: 30,
    borderColor: 'grey',
    borderWidth: 1,
    fontSize: 20,
    marginHorizontal: 15,
    marginBottom: 10,
    color: '#ffffff'
  }
});

export default Register;