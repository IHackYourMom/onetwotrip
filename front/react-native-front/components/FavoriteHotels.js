import React from 'react';
import { ScrollView, TouchableOpacity, ImageBackground,Text, StyleSheet, Alert } from 'react-native';

class FavoriteHotels extends React.Component {
  constructor(props) {
    super(props);
    this._onHotelPress = this._onHotelPress.bind(this);
  }

  _onHotelPress() {
    Alert.alert('Забронировать?');
  }

  render() {
    return (
      <ScrollView style={styles.container}>
        {
          this.props.favorites.map((fav, index) => (
            <TouchableOpacity style={styles.touchable}
              key={index}
              onPress={() => this._onHotelPress()}>
              <ImageBackground style={styles.destinationContainer}
                source={{ uri: fav.imageUrl }}>
                <Text style={styles.additional}>Рейтинг: {fav.rating}</Text>
                <Text style={styles.city}>{fav.name}</Text>
                <Text style={styles.price}>{fav.price}</Text>
              </ImageBackground>
            </TouchableOpacity>
          ))
        }
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#222222'
  },
  touchable: {
    flex: 1,
    margin: 5
  },
  destinationContainer: {
    flex: 1,
    padding: 10
  },
  additional: {
    color: '#ffffff',
    fontSize: 15
  },
  city: {
    color: '#ffffff',
    fontSize: 40,
    fontWeight: '500'
  },
  price: {
    alignItems: 'flex-end',
    color: '#ffffff',
    fontSize: 40
  }
});

export default FavoriteHotels;