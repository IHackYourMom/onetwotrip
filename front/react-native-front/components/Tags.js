import React from 'react';
import { View, Text, Button, SegmentedControlIOS, StyleSheet } from 'react-native';
import Cards from './Cards';

class Tags extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      tagLandscape: undefined,
      cuisine: undefined,
      climate: undefined
    };
  }

  render() {
    return (
      <View style={styles.view}>
        <Text style={styles.viewtitle}>
          Выберите приоритеты для путешествия:
        </Text>
        <Text style={styles.paramtitle}>
          Ландшафт
        </Text>
        <SegmentedControlIOS style={styles.segments}
          values={['Море', 'Горы', 'Равнина']}
          selectedIndex={this.state.tagLandscape}
          onChange={e => this.setState({ tagLandscape: e.nativeEvent.selectedSegmentIndex })}
          tintColor="#fcc62e" />
        <Text style={styles.paramtitle}>
          Кухня
        </Text>
        <SegmentedControlIOS style={styles.segments}
          values={['Восточная', 'Европейская', 'Азиатская', 'Бургер']}
          selectedIndex={this.state.tagKitchen}
          onChange={e => this.setState({ tagKitchen: e.nativeEvent.selectedSegmentIndex })}
          tintColor="#fcc62e" />
        <Text style={styles.paramtitle}>
          Климат
        </Text>
        <SegmentedControlIOS style={styles.segments}
          values={['Жаркий', 'Тропики', 'Умеренный', 'Холодный']}
          selectedIndex={this.state.tagClimate}
          onChange={e => this.setState({ tagClimate: e.nativeEvent.selectedSegmentIndex })}
          tintColor="#fcc62e" />
        <Button onPress={() => this.props.navigator.push(cardsScreen)}
          title="Показать направления"
          color="#fcc62e" />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  view: {
    flex: 1,
    marginTop: 20,
    paddingTop: 20,
    paddingLeft: 10,
    paddingRight: 10,
    backgroundColor: '#222222'
  },
  viewtitle: {
    color: '#fcc62e',
    fontWeight: 'bold',
    fontSize: 25,
    marginBottom: 20
  },
  paramtitle: {
    color: '#fcc62e',
    fontSize: 20,
    marginBottom: 10,
    alignSelf: 'center'
  },
  segments: {
    marginBottom: 40
  }
});

const cardsScreen = {
  component: Cards,
  title: 'Направления',
  navigationBarHidden: false,
  passProps: {
    cards: [
      {
        imageUrl: 'http://image1.thematicnews.com/uploads/topics/preview/00/10/37/12/8e7672590e_256crop.jpg',
        country: 'Германия',
        city: 'Мюнхен',
        dates: '10-17 ноя',
        price: '5.500 ₽'
      },
      {
        imageUrl: 'https://media.ticmate.com/resources/ticmate_live/upload/London_Billetter_Tower_of_London_Main.jpg',
        country: 'Англия',
        city: 'Лондон',
        dates: '1-9 дек',
        price: '5.350 ₽'
      },
      {
        imageUrl: 'https://foto.hrsstatic.com/fotos/0/3/256/256/80/FFFFFF/http%3A%2F%2Ffoto-origin.hrsstatic.com%2Ffoto%2F4%2F0%2F3%2F0%2F403043%2F403043_a_490787.jpg/MDw7bM%2FNOEMbXp04c3lzBw%3D%3D/0%2C0/6/Sarotti_Hoefe-Berlin-Exterior_view-3-403043.jpg',
        country: 'Германия',
        city: 'Берлин',
        dates: '4-11 дек',
        price: '3.200 ₽'
      },
      {
        imageUrl: 'https://static-s.aa-cdn.net/img/gp/20600004652650/Dr60urxAGWz6QBx9FCLfpFprx5tW_DU_-b53H2ZZrZrjAyvG7Tfnp37ggU2yRHJ-Rfnc=w300?v=1',
        country: 'Нидерланды',
        city: 'Амстердам',
        dates: '10-22 дек',
        price: '7.200 ₽'
      },
      {
        imageUrl: 'https://c.tcdn.co/852/9de/8529dee2-38d8-11e6-bbbf-040157cdaf01/channel256.png',
        country: 'Италия',
        city: 'Рим',
        dates: '1-7 дек',
        price: '6.540 ₽'
      }
    ]
  }
};

export default Tags;