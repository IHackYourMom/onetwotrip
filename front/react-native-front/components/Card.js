import React from 'react';
import { View, Text, ImageBackground, StyleSheet } from 'react-native';

class Card extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { imageUrl, country, city, dates, price } = this.props.card;
    return (
      <ImageBackground style={styles.card}
        source={{ uri: imageUrl }}>
        <View style={styles.cardTextContainer}>
          <Text style={styles.additional}>{country}</Text>
          <Text style={styles.city}>{city}</Text>
          <Text style={styles.additional}>{dates}</Text>
        </View>
        <Text style={styles.price}>{price}</Text>
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  card: {
    flex: 1,
    borderRadius: 4,
    borderWidth: 2,
    borderColor: 'grey',
    justifyContent: 'center',
    backgroundColor: 'white'
  },
  cardTextContainer: {
    padding: 10
  },
  additional: {
    color: '#ffffff',
    fontSize: 15
  },
  city: {
    color: '#ffffff',
    fontSize: 40,
    fontWeight: '500'
  },
  price: {
    alignContent: 'center',
    color: '#ffffff',
    fontSize: 40,
    padding: 10,
    fontWeight: '500'
  }
});

export default Card;