import {
  commitMutation,
  graphql,
} from 'react-relay'
import environment from '../Environment'

const mutation = graphql`
  mutation CreateUserMutation($input: CreateUserInput! ){
    createUser(input: $input) {
      user {
        id
        username
        email
      }
    }
  }
`

export default (username, email, password, callback) => {
  const variables = {
    input: {
      username,
      email,
      password,
    },
  }

  commitMutation(
    environment,
      {
        mutation,
        variables,
        onCompleted: () => {
          callback()
        },
        onError: err => console.error(err),
      },
  )
}
