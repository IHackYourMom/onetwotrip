import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import CreateUser from './Components/CreateUser'
import SignIn from './Components/SignIn'

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <p>Sign Up</p>
          <CreateUser />
          <p>Sign In</p>
          <SignIn />
        </header>
      </div>
    );
  }
}

export default App;
