import React, { Component } from 'react'

/* Mutation */
import CreateUserMutation from '../Mutations/CreateUserMutation'

class CreateUser extends Component {


  state = {
    username: '',
    email: '',
    password: '',
  }

  render() {
    return (
      <div>
        <input
          value={this.state.title}
          onChange={(e) => this.setState({username: e.target.value})}
          type='text'
          placeholder="Username"
        />
        <br />
        <input
          value={this.state.title}
          onChange={(e) => this.setState({email: e.target.value})}
          type='text'
          placeholder="Email"
        />
        <br />
        <input
          value={this.state.title}
          onChange={(e) => this.setState({password: e.target.value})}
          type='password'
          placeholder="Password"
        />
        <br />
        <button
          onClick={() => this._createUser()}
        >
          Sign Up
        </button>
      </div>
    )
  }

  _createUser = () => {
    const { username, email, password } = this.state
    CreateUserMutation(username, email, password, () => console.log('Mutation complete'))
  }

}

export default CreateUser
