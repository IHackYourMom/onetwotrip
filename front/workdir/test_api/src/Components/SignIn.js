import React, { Component } from 'react'
import { GC_AUTH_TOKEN } from '../constants'

/* Mutation */
import TokenAuthMutation from '../Mutations/TokenAuthMutation'

class SignIn extends Component {
  state = {
    username: '',
    password: '',
  }

  render() {
    return(
      <div>
        <input
          value={this.state.username}
          onChange={(e) => this.setState({username: e.target.value})}
          type='text'
          placeholder="Username"
        />
        <br />
        <input
          value={this.state.password}
          onChange={(e) => this.setState({password: e.target.value})}
          type='password'
          placeholder="Password"
        />
        <br />
        <button
          onClick={() => this._signInUser()}
        >
          Sign In
        </button>
      </div>
    )
  }

  _signInUser = () => {
    const { username, password } = this.state
    TokenAuthMutation(username, password, (token) =>
    this._saveUserData(token))
  }

  _saveUserData = (token) => {
    localStorage.setItem(GC_AUTH_TOKEN, token)
    console.log(token)
  }

}

export default SignIn
