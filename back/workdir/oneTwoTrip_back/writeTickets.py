import json
from countries.models import Airport, Ticket

with open("tickets.json", "r") as read_file:
    tickets_data = json.load(read_file)

for i in range(len(tickets_data)):
    
    fromAirportName = tickets_data[i]["fromAirport"]
    fromAirportExists = Airport.objects.filter(name=fromAirportName).exists()

    toAirportName = tickets_data[i]["toAirport"]
    toAirportExists = Airport.objects.filter(name=toAirportName).exists()

    minPrice = tickets_data[i]["minPrice"]

    if fromAirportExists and toAirportExists:
        fromAirportPK = Airport.objects.get(name=fromAirportName).pk
        toAirportPK = Airport.objects.get(name=toAirportName).pk
        new_ticket = Ticket(minPrize=minPrice, fromAirport=Airport(pk=fromAirportPK), toAirport=Airport(pk=toAirportPK))
        new_ticket.save()

    print(i, '/', len(tickets_data))
