from django.contrib.auth import get_user_model

import graphene
import django_filters
from graphene_django import DjangoObjectType
from graphene_django.filter import DjangoFilterConnectionField

class UserFilter(django_filters.FilterSet):
    class Meta:
        model = get_user_model()
        fields = ['username', 'email']

class UserNode(DjangoObjectType):
    class Meta:
        model = get_user_model()
        interfaces = (graphene.relay.Node,)

class UserType(DjangoObjectType):
    class Meta:
        model = get_user_model()

class CreateUser(graphene.relay.ClientIDMutation):
    user = graphene.Field(UserNode)

    class Input:
        username = graphene.String(required=True)
        password = graphene.String(required=True)
        email = graphene.String(required=True)

    def mutate_and_get_payload(root, info, **input):
        user = get_user_model()(
                username=input.get('username'),
                email=input.get('email'),
        )

        user.set_password(input.get('password'))
        user.save()

        return CreateUser(user=user)
"""
class Viewer(graphene.ObjectType):
    user = graphene.Field(get_user_model())

    def resolve_user(self, args, context, info):
        if context.user.is_authenticated():
            return context.user
        return None
"""

class Query(graphene.ObjectType):
    user = graphene.relay.Node.Field(UserNode)
    users = DjangoFilterConnectionField(UserNode, filterset_class=UserFilter)

class Mutation(graphene.AbstractType):
    create_user = CreateUser.Field()
