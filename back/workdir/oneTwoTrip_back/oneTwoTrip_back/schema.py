import graphene
import graphql_jwt

import users.schema
import countries.schema

class Query(
    users.schema.Query,
    countries.schema.Query,
    graphene.ObjectType,
):
    pass

class Mutation(
    users.schema.Mutation,
    countries.schema.Mutation,
    graphene.ObjectType,
):
    token_auth = graphql_jwt.relay.ObtainJSONWebToken.Field()
    verify_token = graphql_jwt.relay.Verify.Field()
    refresh_token = graphql_jwt.relay.Refresh.Field()

schema = graphene.Schema(
    query=Query,
    mutation=Mutation,
)
