import json
from countries.models import Country, City, Airport

with open("airports.json", "r") as read_file:
    airport_data = json.load(read_file)

with open("countries.json", "r") as read_file:
    country_data = json.load(read_file)

with open("tickets.json", "r") as read_file:
    tickets_data = json.load(read_file)

for airport in airport_data:
    
    # Code of country
    country_code = airport_data[airport]["country"]

    if airport_data[airport]["iata"] is not None and airport_data[airport]["iata"] != "":
        airport_code = airport_data[airport]["iata"]
    else:
        airport_code = "Bad"

    # City name
    city = airport_data[airport]["city"]

    try:
        country_name = country_data[country_code]

        # If Country exists nothing to do
        if Country.objects.filter(name=country_name).exists():
            pass
        else:
            # Else add new country
            print('Add ', country_name, ' to country DB')
            country = Country(name=country_name)
            country.save()

        # Then we need to add cities to DB
        

        # If City is exist nothing to do
        if City.objects.filter(name=city).exists():
            pass
        elif airport_code=="Bad":
            pass
        else:
            # Else add new city
            try:

                country_name = country_data[country_code]
                print('Add ', city, ' to city DB')
                country_pk = Country.objects.get(name=country_name).pk
                city_obj = City(name=city, country=Country(pk=country_pk))
                city_obj.save()
            except KeyError:
                print(':(')

        if Airport.objects.filter(name=airport_code).exists():
            pass
        elif airport_code=="Bad":
            pass
        else:
            try:
                country_name = country_data[country_code]
                print('Add ', airport_code, ' to airport DB')
                country_pk = Country.objects.get(name=country_name).pk
                city_pk = City.objects.get(name=city).pk

                airport_obj = Airport(name=airport_code, country=Country(pk=country_pk), city=City(pk=city_pk))
                airport_obj.save()
            except KeyError:
                print(':(')

    except KeyError:
        country = Country(name="Unknown City")
        country.save()
