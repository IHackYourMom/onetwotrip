from django.db import models
from django.conf import settings
from tagging.registry import register
from tagging.models import Tag

class Country(models.Model):
    name = models.CharField('Country Name', max_length=50)
    tag_climate = models.CharField('Climate', max_length=50, default="Unknown")
    tag_kitchen = models.CharField('Kitchen', max_length=50, default="Unknown")
    tag_landscape = models.CharField('Landscape',max_length=50, default="Unknown")

register(Country)

class FavouriteTags(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL,
                             on_delete=models.CASCADE)

    tag_climate = models.CharField('Climate', max_length=50)
    tag_kitchen = models.CharField('Kitchen', max_length=50)
    tag_landscape = models.CharField('Landscape', max_length=50)

class City(models.Model):
    name = models.CharField('City Name', max_length=50)
    country = models.ForeignKey('countries.Country',
                                related_name='cities',
                                on_delete=models.CASCADE)


class Airport(models.Model):
    name = models.CharField('Airport Code', max_length=3)
    country = models.ForeignKey('countries.Country',
                                related_name='airportsContry',
                                on_delete=models.CASCADE)

    city = models.ForeignKey('countries.City',
                             related_name='airportsCity',
                             on_delete=models.CASCADE)


class Ticket(models.Model):
    minPrize = models.IntegerField(default=0, blank=False, null=False)
    fromAirport = models.ForeignKey('countries.Airport',
                                    related_name='ticketsFromAirport',
                                    on_delete=models.CASCADE)

    toAirport = models.ForeignKey('countries.Airport',
                                 related_name='ticketsToAirport',
                                 on_delete=models.CASCADE)


class UserLike(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL,
                             on_delete=models.CASCADE)

    ticket = models.ForeignKey('countries.Ticket',
                             related_name='likes',
                             on_delete=models.CASCADE)


class UserDislike(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL,
                             on_delete=models.CASCADE)

    ticket = models.ForeignKey('countries.Ticket',
                             related_name='dislikes',
                             on_delete=models.CASCADE)


class UserDataSet(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL,
                             on_delete=models.CASCADE)

    favourite_tags = models.ForeignKey('countries.FavouriteTags',
                                        related_name='tags',
                                        on_delete=models.CASCADE)

    user_json_path = models.CharField('Data Path', max_length=200)
