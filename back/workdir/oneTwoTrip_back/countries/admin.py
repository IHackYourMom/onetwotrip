from django.contrib import admin

from .models import Country, City, Airport, Ticket, UserLike, UserDislike

admin.site.register(Country)
admin.site.register(City)
admin.site.register(Airport)
admin.site.register(Ticket)
admin.site.register(UserLike)
admin.site.register(UserDislike)
