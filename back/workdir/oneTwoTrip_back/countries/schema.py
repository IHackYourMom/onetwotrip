import graphene
import graphql_relay
import django_filters
from graphene_django import DjangoObjectType
from graphene_django.filter import DjangoFilterConnectionField

from .models import Country, City, UserLike, UserDislike, Airport, Ticket, FavouriteTags
from .models import UserDataSet
# Filter classes

class CountryFilter(django_filters.FilterSet):
    class Meta:
        model = Country
        fields = ['name', 'tag_climate', 'tag_kitchen', 'tag_landscape']


class CityFilter(django_filters.FilterSet):
    class Meta:
        model = City
        fields = ['name', 'country']


class FavouriteTagsFilter(django_filters.FilterSet):
    class Meta:
        model = FavouriteTags
        fields = ['tag_climate', 'tag_kitchen', 'tag_landscape']


class AirportFilter(django_filters.FilterSet):
    class Meta:
        model = Airport
        fields = ['name', 'city']


class TicketFilter(django_filters.FilterSet):
    class Meta:
        model = Ticket
        fields = ['minPrize', 'fromAirport', 'toAirport']

class LikeFilter(django_filters.FilterSet):
    class Meta:
        model = UserLike
        fields = ['ticket', 'user']

class DislikeFilter(django_filters.FilterSet):
    class Meta:
        model = UserDislike
        fields = ['ticket', 'user']

# Node classes for relay queries

class CountryNode(DjangoObjectType):
    class Meta:
        model = Country
        interfaces = (graphene.relay.Node,)

class CityNode(DjangoObjectType):
    class Meta:
        model = City
        interfaces = (graphene.relay.Node,)

class FavouriteTagsNode(DjangoObjectType):
    class Meta:
        model = FavouriteTags
        interfaces = (graphene.relay.Node,)

class AirportNode(DjangoObjectType):
    class Meta:
        model = Airport
        interfaces = (graphene.relay.Node,)

class TicketNode(DjangoObjectType):
    class Meta:
        model = Ticket
        interfaces = (graphene.relay.Node,)

class LikeNode(DjangoObjectType):
    class Meta:
        model = UserLike
        interfaces = (graphene.relay.Node,)

class DislikeNode(DjangoObjectType):
    class Meta:
        model = UserDislike
        interfaces = (graphene.relay.Node,)


class UserDataSet(DjangoObjectType):
    class Meta:
        model = UserDataSet
        interfaces = (graphene.relay.Node,)

# Mutations

class CreateLike(graphene.relay.ClientIDMutation):
    like = graphene.Field(LikeNode)

    class Input:
        ticket_id = graphene.ID()

    def mutate_and_get_payload(root, info, **input):
        user = info.context.user
        id=input.get('ticket_id')
        if user.is_anonymous:
            raise Exception('Not logged in!')

        _, pk = graphql_relay.from_global_id(id)

        ticket = Ticket.objects.get(pk=pk)
        if not ticket:
            raise Exception('Invalid ticket')

        like = UserLike(
            user=user,
            ticket=ticket,
        )
        like.save()

        return CreateLike(like=like)

class CreateDislike(graphene.relay.ClientIDMutation):
    dislike = graphene.Field(DislikeNode)

    class Input:
        ticket_id = graphene.ID()

    def mutate_and_get_payload(root, info, **input):
            user = info.context.user
            id=input.get('ticket_id')
            if user.is_anonymous:
                raise Exception('Not logged in!')

            _, pk = graphql_relay.from_global_id(id)

            ticket = Ticket.objects.get(pk=pk)
            if not ticket:
                raise Exception('Invalid ticket')

            dislike = UserDislike(
                user=user,
                ticket=ticket,
            )
            dislike.save()

            return CreateDislike(dislike=dislike)

class CreateFavouriteUserTags(graphene.relay.ClientIDMutation):
    favourite_tags = graphene.Field(FavouriteTagsNode)

    class Input:
        tag_climate = graphene.String()
        tag_kitchen = graphene.String()
        tag_landscape = graphene.String()

    def mutate_and_get_payload(root, info, **input):
        user = info.context.user
        if user.is_anonymous:
            raise Exception('Not logged in!')

        favourite_tags = FavouriteTags(
            user=user,
            tag_climate=input.get('tag_climate'),
            tag_kitchen=input.get('tag_kitchen'),
            tag_landscape=input.get('tag_landscape'),
        )
        favourite_tags.save()

        return CreateFavouriteUserTags(favourite_tags=favourite_tags)
"""
class CreateUserDataSet(graphene.relay.ClientIDMutation):

    def mutate_and_get_payload(root, info, **input):
        user = info.context.user
        if user.is_anonymous:
            raise Exception('Not logged in!')

    tickets_id_arr = []

    for in range(0, 10):
"""

class Query(graphene.AbstractType):
    country = graphene.relay.Node.Field(CountryNode)
    countries = DjangoFilterConnectionField(CountryNode, filterset_class=CountryFilter)

    city = graphene.relay.Node.Field(CityNode)
    cities = DjangoFilterConnectionField(CityNode, filterset_class=CityFilter)

    airport = graphene.relay.Node.Field(AirportNode)
    airports = DjangoFilterConnectionField(AirportNode, filterset_class=AirportFilter)

    ticket = graphene.relay.Node.Field(TicketNode)
    tickets = DjangoFilterConnectionField(TicketNode, filterset_class=TicketFilter)

    like = graphene.relay.Node.Field(LikeNode)
    likes = DjangoFilterConnectionField(LikeNode, filterset_class=LikeFilter)

    dislike = graphene.relay.Node.Field(DislikeNode)
    dislikes = DjangoFilterConnectionField(DislikeNode, filterset_class=DislikeFilter)

    favourite_tags = DjangoFilterConnectionField(FavouriteTagsNode, filterset_class=FavouriteTagsFilter)

class Mutation(graphene.AbstractType):
    create_like = CreateLike.Field()
    create_dislike = CreateDislike.Field()
    create_favourite_tags = CreateFavouriteUserTags.Field()
